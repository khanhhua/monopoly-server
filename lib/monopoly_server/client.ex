defmodule MonopolyServer.Client do
  @behaviour :gen_statem
  require Record
  require MonopolyServer.CmdParser, as: Parser
  require MonopolyServer.Game, as: Game

  Record.defrecord :client_state, [socket: nil, username: nil, game_pid: nil]

  # Public API
  def get_name(pid) do
    :gen_statem.call(pid, :get_name)
  end

  # Gen Statemachine Callbacks
  def start_link(client_socket) do
    :gen_statem.start_link(__MODULE__, client_socket, [])
  end

  def init(client_socket) do
    Process.flag(:trap_exit, true)
    :inet.setopts(client_socket, [:binary, {:active, true}, {:packet, :raw}])
    {:ok, :in_lobby, client_state(socket: client_socket)}
  end

  def terminate(reason, _state, _data) do
    IO.puts("Client terminated")
    IO.inspect reason, label: "Reason"
    :ok
  end

  def code_change(_vsn, state, data, _extra) do
    {:ok, state, data}
  end

  def callback_mode do
    # Very important
    :state_functions
  end

  # State Callback Methods
  def in_lobby(:cast, {:tell, msg}, state) do
    socket = client_state(state, :socket)
    :gen_tcp.send(socket, "#{msg}\n")
    {:keep_state, state}
  end
  def in_lobby({:call, from_ref}, message, state) do
    handle_common({:call, from_ref}, message, state)
  end
  def in_lobby(:info, {:tcp, socket, cmdline}, state) do
    IO.inspect cmdline, label: "In Lobby CMD"
    case Parser.parse(cmdline) do
      {:ok, :login, username} ->
        next_state = client_state(state, username: username)
        {:next_state, :in_lobby, next_state}
      {:ok, :create} ->
        username = client_state(state, :username)
        IO.inspect username, label: "username"
        case Game.start_link(username) do
          {:ok, game_pid} ->
            next_state = client_state(state, game_pid: game_pid)
            IO.inspect game_pid, label: "game_pid"
            {:next_state, :in_game, next_state}
          _ -> :keep_state_and_data
        end
      {:ok, :join, game_name} ->
        case Registry.lookup(Registry.ActiveGames, game_name) do
          [{game_pid, nil}] ->
            IO.inspect game_pid, label: "game_pid"
            username = client_state(state, :username)
            case Game.join(game_pid, username) do
              :ok ->
                next_state = client_state(state, game_pid: game_pid)
                {:next_state, :in_game, next_state}
              _ -> :keep_state_and_data
            end
          _ -> :keep_state_and_data
        end
      {:ok, cmd} ->
        case apply(__MODULE__, cmd, [state]) do
          {:ok, reply} -> reply_custom(socket, reply)
          {:error, _error} -> reply_bad_command(socket)
        end
        :keep_state_and_data
      {:ok, cmd, arg} ->
        case apply(__MODULE__, cmd, [arg, state]) do
          {:ok, reply} ->
            reply_custom(socket, reply)
          {:error, _error} -> reply_bad_command(socket)
        end
        :keep_state_and_data
      {:error, error} ->
        IO.inspect error, label: "error"
        reply_bad_command(socket)

        {:next_state, :in_lobby, state}
      _ -> {:next_state, :in_lobby, state}
    end
  end
  def in_lobby(:info, {:tcp_closed, _socket}, state) do
    IO.puts("Socket closed")
    {:stop, :normal, state}
  end
  def in_lobby(:info, {:tcp_error, _socket, reason}, state) do
    IO.puts("Connection closed due to #{reason}")
    {:stop, :normal, state}
  end

  def in_game(:info, {:tcp, socket, cmdline}, state) do
    IO.inspect cmdline, label: "In Game CMD"

    case Parser.parse(cmdline) do
      {:ok, :start} ->
        IO.puts "Starting game"
        case Game.start_game(client_state(state, :game_pid)) do # Start your own game
          :ok -> reply_ok(socket)
          {:error, _err} -> reply_bad_command(socket)
        end
        {:keep_state, state}
      {:ok, :quit} ->
        IO.puts "Quiting game"
        game_pid = client_state(state, :game_pid)

        case Game.is_owner(game_pid) do
          {:ok, true} ->
            case Game.force_stop(game_pid) do
              :ok -> reply_ok(socket)
              {:error, _error} -> reply_bad_command(socket)
            end
          {:ok, false} ->
            Game.leave(game_pid)
            IO.puts "LEAVE game only"
          {:error, error} -> IO.inspect error, label: "error"
        end
        next_state = client_state(state, game_pid: nil)
        {:next_state, :in_lobby, next_state}
      {:ok, cmd} ->
        IO.inspect {cmd}, label: "In Game CMD"
        {:keep_state, state}
      {:ok, cmd, arg} ->
        IO.inspect {cmd, arg}, label: "In Game CMD"
        {:keep_state, state}
      {:error, err} ->
        IO.inspect err, label: "Invalid command"
        reply_bad_command(socket)
        {:keep_state, state}
    end
  end
  def in_game({:call, from_ref}, message, state) do
    handle_common({:call, from_ref}, message, state)
  end

  # Internal Elixir calls
  def handle_common({:call, from_ref}, method_name, state) do
    case method_name do
      :get_name -> {:keep_state_and_data, [{:reply, from_ref, {:ok, client_state(state, :username)}}]}
      _ -> {:keep_state_and_data}
    end
  end
  # TCP fed command handlers
  def show("games", _state) do
    games = Registry.select(Registry.ActiveGames, [{{:"$1", :_, :_}, [], [:"$1"]}])
    IO.inspect games, label: "games"
    {:ok, games}
  end

  # Private functions
  defp write(socket, message) do
    :gen_tcp.send(socket, message)
  end

  defp reply_bad_command(socket) do
    write(socket, "Bad command\n")
  end

  defp reply_ok(socket) do
    write(socket, "OK\n")
  end

  defp reply_custom(socket, list) when is_list(list) do
    write(socket, "#{Enum.join(list, "\n")}\n")
  end
  defp reply_custom(socket, data), do: write(socket, :io_lib.format("%s", [data]))
end
