defmodule MonopolyServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    ip = {127,0,0,1}
    port = 9000

    children = [
      # Starts a worker by calling: MonopolyServer.Worker.start_link(arg)
      {Registry, [keys: :unique, name: Registry.ActiveGames]},
      {MonopolyServer.Server, [ip, port]}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MonopolyServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
