defmodule MonopolyServer.Game do
  @behaviour :gen_statem
  require Record

  Record.defrecord :player_data, [pid: nil, name: nil, cash: nil]
  Record.defrecord :game_data, [players: []]

  # PUBLIC API
  def start_link(owner_name) do
    name = {:via, Registry, {Registry.ActiveGames, owner_name}}
    :gen_statem.start_link(name, __MODULE__, [self(), owner_name], [])
  end

  def is_owner(game_pid) do
    :gen_statem.call(game_pid, :is_owner)
  end

  def force_stop(game_pid) do
    :gen_statem.call(game_pid, :force_stop)
  end

  def start_game(game_pid) do
    :gen_statem.call(game_pid, :start_game)
  end

  def join(game_pid, name) do
    :gen_statem.call(game_pid, {:join, name})
  end

  def leave(game_pid) do
    :gen_statem.call(game_pid, :leave)
  end

  # OTP CALLBACKS
  def init([owner_pid, owner_name]) do
    IO.puts "Initializing..."
    players = [
      player_data(pid: owner_pid, name: owner_name, cash: 1500)
    ]

    {:ok, :awaiting_players, game_data(players: players)}
  end

  def callback_mode do
    # Very important
    :state_functions
  end

  def awaiting_players({:call, from_ref}, {:join, name}, state) do
    IO.puts "Player requesting to join game..."
    player_pid = get_pid(from_ref)

    IO.inspect name, label: "name"
    p = player_data(pid: player_pid, name: name, cash: 1500)
    players = Enum.concat(game_data(state, :players), [p])

    {:keep_state, game_data(state, players: players), [{:reply, from_ref, :ok}]}
  end
  def awaiting_players({:call, from_ref}, :start_game, state) do
    [player] = game_data(state, :players) # The game owner is always the first player
    player_pid = player_data(player, :pid)
    case player_pid == get_pid(from_ref) do
      true -> {:next_state, :playing, state, [{:reply, from_ref, :ok}]}
      false -> {:keep_state, state, [{:reply, from_ref, {:error, :permission}}]}
    end
  end
  def awaiting_players(event_type, event_content, state), do: handle_common(event_type, event_content, state)

  def playing({:call, from_ref}, :leave, state) do
    player_pid = get_pid(from_ref)
    player_index = state
        |> game_data(:players)
        |> Enum.find_index(&(player_data(&1, :pid) == player_pid))
    case player_index != nil do
      true ->
        next_players = state
            |> game_data(:players)
            |> List.delete_at(player_index)
        {:keep_state, game_data(state, players: next_players), [{:reply, from_ref, :ok}]}
      false -> {:keep_state_and_data, [{:reply, from_ref, {:error, :permission}}]}
    end
  end
  def playing(event_type, event_content, state), do: handle_common(event_type, event_content, state)

  def stopped(event_type, event_content, state), do: handle_common(event_type, event_content, state)

  def handle_common({:call, from_ref}, :is_owner, state) do
    [player] = game_data(state, :players)
    {:keep_state_and_data, [{:reply, from_ref, {:ok, get_pid(from_ref) == player_data(player, :pid)}}]}
  end
  def handle_common({:call, from_ref}, :force_stop, state) do
    IO.puts "Force stopping..."
    [player] = game_data(state, :players)

    case get_pid(from_ref) == player_data(player, :pid) do
      false -> {:keep_state_and_data, [{:reply, from_ref, {:error, :permission}}]}
      true -> {:next_state, :stopped, state, [{:reply, from_ref, :ok}]}
    end
  end
  def handle_common({_, from_ref}, _event_content, _state) do
    IO.puts "Invalid action", label: "Game"
    {:keep_state_and_data, [{:reply, from_ref, {:error, :bad_command}}]}
  end

  defp get_pid({from, _ref}), do: from
end
