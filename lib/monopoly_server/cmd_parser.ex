defmodule MonopolyServer.CmdParser do
  @doc """
  Parse `line` into `command`

  ## Examples
  iex> MonopolyServer.CmdParser.parse(<<"login tom">>)
  {:ok, :login, "tom"}

  iex> MonopolyServer.CmdParser.parse(<<"show games">>)
  {:ok, :show, "games"}

  iex> MonopolyServer.CmdParser.parse(<<"show players">>)
  {:ok, :show, "players"}

  iex> MonopolyServer.CmdParser.parse(<<"join tom">>)
  {:ok, :join, "tom"}

  iex> MonopolyServer.CmdParser.parse(<<"draw chance">>)
  {:ok, :draw, "chance"}

  iex> MonopolyServer.CmdParser.parse(<<"draw risk">>)
  {:ok, :draw, "risk"}

  iex> MonopolyServer.CmdParser.parse(<<"buy property">>)
  {:ok, :buy, "property"}

  iex> MonopolyServer.CmdParser.parse(<<"buy hotel">>)
  {:ok, :buy, "hotel"}

  iex> MonopolyServer.CmdParser.parse(<<"yield turn">>)
  {:ok, :yield, "turn"}

  iex> MonopolyServer.CmdParser.parse(<<"quit">>)
  {:ok, :quit}
  """
  def parse(<<"login", args :: binary>>) do
    case String.split(args) do
      [username] -> {:ok, :login, username}
      _ -> {:error, :bad_command}
    end
  end
  def parse(<<"show", args :: binary>>) do
    case String.split(args) do
      [what] when what == "games" or what == "players" -> {:ok, :show, what}
      _ -> {:error, :bad_command}
    end
  end
  def parse(<<"create", _args :: binary>>), do: {:ok, :create}
  def parse(<<"join", args :: binary>>) do
    case String.split(args) do
      [game_id] -> {:ok, :join, game_id}
      _ -> {:error, :bad_command}
    end
  end
  def parse(<<"start", _args :: binary>>), do: {:ok, :start}
  def parse(<<"spin", _args :: binary>>), do: {:ok, :spin}
  def parse(<<"draw", args :: binary>>) do
    case String.split(args) do
      [card_type] when card_type == "chance" or card_type == "risk" -> {:ok, :draw, card_type}
      _ -> {:error, :bad_command}
    end
  end
  def parse(<<"buy", args :: binary>>) do
    case String.split(args) do
      [what] when what == "hotel" or what == "property" -> {:ok, :buy, what}
      _ ->  {:error, :bad_command}
    end
  end
  def parse(<<"yield", args :: binary>>) do
    case String.split(args) do
      [what] when what == "turn" -> {:ok, :yield, what}
      _ ->  {:error, :bad_command}
    end
  end
  def parse(<<"quit", _args :: binary>>), do: {:ok, :quit}
  def parse(_), do: {:error, :bad_command}
end
